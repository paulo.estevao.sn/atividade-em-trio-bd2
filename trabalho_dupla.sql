-- Questão 1.a
DELIMITER $$;
DROP PROCEDURE IF EXISTS sp_Demite 

$$ CREATE PROCEDURE sp_Demite (funcionario_nome VARCHAR(100)) 
BEGIN
  UPDATE Funcionario
  SET status = 'Demitido'
  WHERE nome = funcionario_nome;
END $$ 
DELIMITER ;

CALL sp_Demite ('HENRIQUE SOUZA MARCOS');


